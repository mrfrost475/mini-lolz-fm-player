# Mini LOLZ FM Player

## Description
This project was created in your free time to listen to your favorite FM radio without having to download a heavy browser and spend a lot of RAM, it is written based on PyQt5 and consumes only about 40 megabytes of RAM and very little network traffic, the interface has an indication: playing / stopped, as well as buttons for these actions and volume control (not all Windows), but only the audio stream of the program itself :)

## How to use?
Ok bro, there are 2 launch options:
- Run main.py via cmd
- Run my compiled exe file

If you want to run main.py directly then please install the required libraries
- `pip install PyQt5`
- `pip install python-vlc`

## Screenhots
![](./screenshot1.png)
![](./screenshot2.png)
![](./screenshot3.png)

## Download lastest release
- **[Python version](lolzfm.py)**
- **[Compiled EXE](lolzfm.exe)**
