import sys
import vlc
from PyQt5 import QtWidgets, QtGui, QtCore


class Player(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Player, self).__init__(parent)
        self.setWindowTitle("LOLZ FM")
        self.setFixedSize(250, 150)  # Установка фиксированных размеров окна

        # Устанавливаем иконку приложения
        app_icon = QtGui.QIcon("icon.ico")
        self.setWindowIcon(app_icon)

        # Создаем кнопку "Play"
        self.play_button = QtWidgets.QPushButton("Играть")
        self.play_button.clicked.connect(self.play)

        # Создаем кнопку "Pause"
        self.pause_button = QtWidgets.QPushButton("Пауза")
        self.pause_button.clicked.connect(self.pause)

        # Создаем метку для отображения состояния воспроизведения
        self.status_label = QtWidgets.QLabel("")

        # Создаем регулятор громкости
        self.volume_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.volume_slider.setMinimum(0)
        self.volume_slider.setMaximum(100)
        self.volume_slider.setValue(50)
        self.volume_slider.setTickInterval(10)
        self.volume_slider.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.volume_slider.valueChanged.connect(self.change_volume)

        # Создаем кнопку для открытия всплывающего сообщения
        self.popup_button = QtWidgets.QPushButton("Информация")
        self.popup_button.clicked.connect(self.show_popup)

        # Создаем макет для размещения кнопок, регулятора громкости и метки
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.play_button)
        layout.addWidget(self.pause_button)
        layout.addWidget(self.volume_slider)
        layout.addWidget(self.status_label)
        layout.addWidget(self.popup_button)
        self.setLayout(layout)

        # Создаем экземпляр плеера VLC
        self.media_player = vlc.MediaPlayer()
        self.media_player.audio_set_volume(50)

    def play(self):
        # Создаем медиа-объект для URL-адреса потока
        media = self.media_player.set_media(vlc.Media("https://listen1.myradio24.com/lolz"))

        # Воспроизводим поток
        self.media_player.play()

        # Обновляем метку со статусом воспроизведения
        self.status_label.setText("Играет...")

    def pause(self):
        # Останавливаем воспроизведение потока
        self.media_player.pause()

        # Обновляем метку со статусом воспроизведения
        self.status_label.setText("Остановлен")

    def change_volume(self, value):
        # Устанавливаем громкость воспроизведения
        self.media_player.audio_set_volume(value)

    def show_popup(self):
        # Создаем всплывающее сообщение
        message_box = QtWidgets.QMessageBox(self)
        message_box.setWindowTitle("Информация")
        message_box.setText("MiniFM Player for LOLZ FM by mrfrost475 and ChatGPT")
        message_box.setIcon(QtWidgets.QMessageBox.Information)

        # Добавляем кнопку "Ок" для закрытия сообщения
        message_box.addButton(QtWidgets.QMessageBox.Ok)
        message_box.setDefaultButton(QtWidgets.QMessageBox.Ok)


        # Отображаем сообщение
        message_box.exec_()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    player = Player()
    player.show()
    sys.exit(app.exec_())
